#| This file is part of dunno
   Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>|#

(in-package #:dunno)

(defvar *config-defs* (make-hash-table)
  "Kind of sucks that I have to go to hashtables for this but, unless there's a way to put this state inside a class,
then this is how things have to be.

Stores config-definitions defined-with define-config.")

(defmethod find-config-definition ((designator symbol))
  (gethash designator *config-defs*))

(defclass config-definition ()
  ((%config-options :accessor config-options
                    :initarg :config-options
                    :type list)

   (%parent-configs :accessor parent-configs
                    :initarg :parent-configs
                    :type list
                    :documentation "the symbols naming the parent config-definitions.")

   (%config-name :accessor config-name
                 :initarg :config-name
                 :type symbol
                 :documentation "A name naming the config-definition name.")))

(defgeneric new-config (definition alist &rest meta-options)
  (:documentation "create a config from the definition and alist"))
(defgeneric find-option (key definition)
  (:documentation "return the option-definition for the given key if present."))
(defgeneric ensure-option (definition key association)
  (:documentation "ensure that the option for the config has been applied."))
(defgeneric ensure-meta-option (definition new-config-instance key meta-option)
  (:documentation "take action for the meta option, e.g. var to bind a new config instance to."))
(defgeneric ensure-class-for-config (definition)
  (:documentation "create a class for the config-definition."))
(defgeneric direct-slots-for-config (definition)
  (:documentation "return the direct slots from a config-definition, ready to be passed into amop:canonicalize-direct-slots"))
(defmethod new-config ((definition config-definition) alist &rest meta-options)
  (let ((initargs
         (reduce #'append
                 (mapcar (lambda (a)
                           (ensure-option definition (car a) a))
                         alist))))

    (let ((instance (apply #'make-instance (config-name definition) initargs)))
      (mapc (lambda (a)
              (ensure-meta-option definition instance (car a) a))
            meta-options)
      instance)))

(defmethod ensure-meta-option ((definition config-definition) instance (key (eql :dynamic-var)) meta-option)
  (set (cdr meta-option) instance))

(defmethod find-option (key (definition config-definition))
  (or (assoc key (config-options definition))
       (let ((found? nil)
             (parents (parent-configs definition))
             (result nil))
         (loop :for p :in parents :while (not found?) :do
              (let ((option (find-option key p)))
                (when option
                  (setf found? t)
                  (setf result option))))
         result)))

(defmethod find-option (key (definition symbol))
  (let ((config (find-config-definition definition)))
    (when config (find-option key config))))


(defmethod ensure-option ((definition config-definition) key association)
  (let ((option (cdr (find-option key definition))))
    ;; determine the option type.
    (cond ((getf option :dynamic-var)
           (set (getf option :dynamic-var) (cdr association))
           nil)

          ((getf option :name) ; assume the key is the initarg when we do this.
           (list key (cdr association))))))

(defmethod direct-slots-for-config ((definition config-definition))
  (remove-if #'null
             (loop :for s :in (config-options definition) :collecting
                  (let ((slot-name (getf (cdr s) :name)))
                    (when slot-name
                      (let ((s (copy-list s)))
                        (let ((initarg (pop s)))
                          (remf s :name) (push initarg s) (push :initarg s) (push slot-name s))
                        s))))))
(defun make-class (class-name superclasses slots &rest options &key &allow-other-keys)
  (eval `(defclass ,class-name ,superclasses ,slots ,@options)))

(defmethod ensure-class-for-config ((definition config-definition))
  (funcall #'make-class
   (config-name definition)
   (mapcar #'config-name (parent-configs definition))
   (direct-slots-for-config definition)))

(defmacro define-config (name (&rest parent-configs) direct-options)
  "define a config-definition and config function with the given name.
To be funcalled with an alist with a key each config-option specified in the direct-options.."
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (let ((config-def (make-instance 'config-definition :config-options ',direct-options :parent-configs ',parent-configs
                                      :config-name ',name)))
       (setf (gethash ',name *config-defs*) config-def)
       (ensure-class-for-config config-def)
       (defun ,name (alist &rest meta-options)
         (apply #'new-config config-def alist meta-options)))))
