(asdf:defsystem "dunno"
    :version "0.0"
    :author "Gnuxie <Gnuxie@protonmail.com>"
    :license "Mozilla Public License Version 2.0"
    :depends-on ("closer-mop")
    :components ((:file "package")
                 (:file "dunno"))
    :description "Very WIP Adaptable application config library")
