# dunno

easy application configuration library.

# Overview

## history

I made dunno for [luna](https://gitlab.com/Gnuxie/luna/) because I was not happy
with what was already available for configuring apps in CL. 
One issue is lots of libraries have a bunch of dynamic-vars that need to be set
for custom behaviour and it can be tricky to keep track of them.
I assume most people configure apps just by doing a few
`(setf foo:bar baz)` in a file that's loaded before they start it, which
isn't all that friendly to someone who hasn't touched lisp before.

## what's it do

`dunno:define-config` can be used to define a class representing a config,
then defines a function with the same name to be used to instantiate it.
This makes a fairly readable looking config when it is called by a user.

### example

Say for some reason we had a library to make twitter bots and we want people
to be able to set drakmas user-agent or change a username...

```
(dunno:define-config my-client-config ()
  ((:user-agent :dynamic-var drakma:*default-user-agent*)
   (:username :name username :accessor username
              :initform "meow")))
```

We can tell dunno to bind to `drakma:*default-user-agent*` when it discovers the
option `:user-agent`.

We also tell it to make a slot called `username` in the configuration class
and bind it when we discover the option `:username`. We also get to give it everything
you would normally with `defclass`, the only difference is the `:name` that tells
dunno what the slot-name should be.

To then create an instance of `my-client-config` we can do this:

```
(my-client-config
  '((:user-agent . "Meowz very cool client application")
    (:username . "Meowy"))
  '(:dynamic-var . *meow-client-config*))
```

This will then crate an my-client-config instance with the username slot bound
to `"Meowy"`, bind the drakma user-agent and then also bind `*meow-client-config*`
to the instance.

The meta options list was added to try reduce as much noise as possible and make
the config as easy to work with for a non-lisper as possible. But at this point
maybe it would have been wiser to just use json or something. Either way, I tried.
The function will still return the instance of the config if you don't want that.

The alist you pass to the config will need to use dotted pairs for values,
since I expect someone to want to be able to pass lists into the config function.

Maybe I will allow users to define the function used to get the value from the assoc
list for each option in future.
