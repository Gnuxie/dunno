#| This file is part of dunno
   Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>|#

(defpackage #:dunno.test
  (:use #:cl #:parachute))

(in-package #:dunno.test)

(defvar *my-config* nil)
(define-test find-option
  (dunno:define-config parent ()
    ((:meow :dynamic-var *meow*)))

  (dunno:define-config child (parent)
    ())

  (true
   (not (null (dunno:find-option :meow
                                 (dunno:find-config-definition 'child))))))

(define-test ensure-class-for-config
  
  (dunno:define-config poo ()
    ((:cheese :name cheese
              :accessor cheese
              :initform "cheese")))

  (is string= "cheese" (cheese (poo nil)))
  (is string= "wheel" (cheese (poo '((:cheese . "wheel")) '(:dynamic-var . *my-config*))))

  (is string= "wheel" (cheese *my-config*)))

