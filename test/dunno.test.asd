(asdf:defsystem "dunno.test"
  :version "0.0"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Mozilla Public License Version 2.0"
  :depends-on ("dunno" "parachute")
  :components ((:file "test")))
