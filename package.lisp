(defpackage #:dunno
  (:use #:cl)
  (:export
   #:config-definition
   #:new-config
   #:ensure-option
   #:define-config
   #:find-config-definition
   #:find-option))
